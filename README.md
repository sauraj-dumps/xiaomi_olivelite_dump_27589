## olivelite-user 10 QKQ1.191014.001 V12.5.1.0.QCPINXM release-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: olivelite
- Brand: Xiaomi
- Flavor: lineage_olivelite-userdebug
- Release Version: 11
- Id: RQ3A.210905.001
- Incremental: eng.mirror.20210921.092920
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 420
- Fingerprint: Xiaomi/olivelite/olivelite:10/QKQ1.191014.001/V12.5.1.0.QCPINXM:user/release-keys
- OTA version: 
- Branch: olivelite-user-10-QKQ1.191014.001-V12.5.1.0.QCPINXM-release-keys
- Repo: xiaomi_olivelite_dump_27589


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
